# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :ios do
	def install_pods
		cocoapods(
			clean: true,
			podfile: "Podfile",
			try_repo_update_on_error: true
			)
	end

  # =======================================
  # ======= Download Certificates =========
  # =======================================

  desc "Download Development Certificates And Profiles"
  lane :download_development_certificates do
  	match_for_all(account_type: "development", readonly: true)
  end

  desc "Download Distribution Certificates And Profiles"
  #readonly 為 false 會建立新的憑證，true 則不會
  lane :download_distribution_certificates do
  	match_for_all(account_type: "appstore", readonly: true) 
  end

  private_lane :match_for_all do |options|
  	account_type = options[:account_type]
  	readonly = options[:readonly]
	# force_for_new_devices => 有新裝置時會重新生成 provisioning profile
	match(type: account_type, 
		  force_for_new_devices: true, 
		  readonly: readonly)
  end

  # =======================================
  # ======= Build ipa to local file =======
  # =======================================
 lane :dev_beta do 
  	download_development_certificates
 	build(build_type: 'development', ipa_name: 'DEV')
  	# 在 macOS 通知欄發送通知
 	notification(subtitle: "打包完成", message: "已經打包在本地了")
 end  

 lane :release_beta do 
	download_distribution_certificates
	build(build_type: 'app-store', ipa_name: 'Release')
 end

 private_lane :build do |options|
	build_number = Time.new.strftime("%y%m%d%H%M")
	build_type = options[:build_type]
	ipa_name = ENV["XCODE_SCHEME"] + "-" + options[:ipa_name]
	configure = build_type == 'development' ? 'Debug': 'Release'
	puts "app build 🏄🏄🏄"
	puts "build_type: " + build_type
	puts "build_number: " + build_number
	puts "ipa_name: " + ipa_name

	increment_build_number(build_number: build_number)
	gym(clean: true,
		configuration: configure,
		scheme: ENV["XCODE_SCHEME"],
		export_method: build_type,
		output_directory: ENV["EXPORT_OUTPUT_DIRECTORY"],
		output_name: ipa_name,
        silent:true,#隱藏沒有必要的資訊
        )

 end

  # =======================================
  # ======= Upload IPA to AppStore ========
  # =======================================

 lane :submit_appstore_review do
  	build(build_type: 'app-store', ipa_name: 'Release')
  	deliver
 end

   # =======================================
  # ======= Upload IPA to TestFlight ======
  # =======================================
 desc "Submit a new Beta Build to Apple TestFlight"
 lane :beta do
 	build_number = Time.new.strftime("%y%m%d%H%M")
  	increment_build_number(
  		build_number: build_number # 也可以使用 latest_testflight_build_number + 1 
  		)
  	gym(project: "FastlaneTutorial.xcodeproj",
  		scheme: ENV["XCODE_SCHEME"],
  		configuration: "Release")
    # upload to testflight
    pilot(
    	beta_app_review_info: {
    	contact_email: "email@email.com",
    	contact_first_name: "Connect",
    	contact_last_name: "API",
    	contact_phone: "5558675309",
    	demo_account_name: "demo@email.com",
    	demo_account_password: "connectapi",
    	notes: "this is review note for the reviewer <3 thank you for reviewing"
    },
    localized_app_info: {
    	"zh-Hant": {
    		feedback_email: "default@email.com",
    		marketing_url: "https://example.com/marketing-defafult",
    		privacy_policy_url: "https://example.com/privacy-defafult",
    		description: "Default description"
    	}
    },
    localized_build_info: {
    	"zh-Hant": {
    		whats_new: "Default changelog"
    	}
    })
 end

end
