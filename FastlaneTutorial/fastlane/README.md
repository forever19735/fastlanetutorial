fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios download_development_certificates
```
fastlane ios download_development_certificates
```
Download Development Certificates And Profiles
### ios download_distribution_certificates
```
fastlane ios download_distribution_certificates
```
Download Distribution Certificates And Profiles
### ios dev_beta
```
fastlane ios dev_beta
```

### ios release_beta
```
fastlane ios release_beta
```

### ios build
```
fastlane ios build
```

### ios submit_appstore_review
```
fastlane ios submit_appstore_review
```

### ios beta
```
fastlane ios beta
```
Submit a new Beta Build to Apple TestFlight

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
